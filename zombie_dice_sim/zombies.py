import random

class Zombie:
    def __init__(self, name = 'Unnamed zombie'):
        self.name = name

    def new_game(self):
        pass

    def new_turn(self):
        pass

    def turn(self, table):
        pass

class MinNumShotgunsThenStopsZombie(Zombie):
    def __init__(self, name, min_shotguns = 2):
        self.name = name
        self.min_shotguns = min_shotguns

    def turn(self, table):
        shotguns = 0
        while shotguns < self.min_shotguns:
            state = table.roll()
            shotguns = len(state['shotguns'])

class CoinFlipZombie(Zombie):
    def __init__(self, name):
        self.name = name

    def turn(self, table):
        while True:
            table.roll()
            if random.randint(0, 1) == 0:
                break