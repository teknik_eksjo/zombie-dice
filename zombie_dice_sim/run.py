from zombies import *
from zombiedice import Game

zombies = [MinNumShotgunsThenStopsZombie('Tank'), 
           MinNumShotgunsThenStopsZombie('Dozer', 1),
           CoinFlipZombie('Apoc')]

print 'Zombies {} and {} playing'.format(', '.join([zombie.name for zombie in zombies[:-1]]), zombies[-1].name)

game = Game(zombies)
scores = game.run()

for name, score in scores.items():
    print 'Player {} scored {}.'.format(name, score)