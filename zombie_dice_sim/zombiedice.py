import logging, random

logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')

RED = 'red'
YELLOW = 'yellow'
GREEN = 'green'
SHOTGUN = 'shotgun'
BRAIN = 'brain'
FEET = 'feet'

class DiceException(Exception):
    pass

class ShotgunException(Exception):
    pass

class GameException(Exception):
    pass

class Outcome:
    def __init__(self, symbol, color):
        self.symbol = symbol
        self.color = color

    def __str__(self):
        return '<Outcome: {}, {}>'.format(self.symbol, self.color)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self == other

class Dice:
    def __init__(self, color, rng = None):
        self.rng = rng if rng else random.Random()
        self.color = color
    
    def __str__(self):
        return '<Dice: {}>'.format(self.color)

    def roll(self):
        r = self.rng.randint(1, 6)

        if self.color == GREEN:
            if r in (1, 2, 3):
                return Outcome(BRAIN, GREEN)
            elif r in (4, 5):
                return Outcome(FEET, GREEN)
            elif r == 6:
                return Outcome(SHOTGUN, GREEN)
        elif self.color == YELLOW:
            if r in (1, 2):
                return Outcome(BRAIN, YELLOW)
            elif r in (3, 4):
                return Outcome(FEET, YELLOW)
            elif r in (5, 6):
                return Outcome(SHOTGUN, YELLOW)
        elif self.color == RED:
            if r == 1:
                return Outcome(BRAIN, RED)
            elif r in (2, 3):
                return Outcome(FEET, RED)
            elif r in (4, 5, 6):
                return Outcome(SHOTGUN, RED)

class Table:
    def __init__(self, rng = None):
        self.rng = rng if rng else random.Random()
        self.dice = [Dice(GREEN) for i in range(6)] + [Dice(YELLOW) for i in range(4)] + [Dice(RED) for i in range(3)]
        self.reroll = [] # Store dice that showed outcome feet and should be rerolled.
        self.reusable_dice = [] # Store dice that showed outcome brains and could be reused.
        self.brains = []
        self.feet = []
        self.shotguns = []

    def roll(self):
        if len(self.shotguns) >= 3:
            logging.debug(ShotgunException('Too many shotguns to continue.'))
            return None

        if len(self.dice) < 3:
            self.dice.extend(self.reusable_dice) # Lets hope it gets us at least 3 dice...
            self.reusable_dice = []

        if len(self.dice) < 3:
            raise DiceException('Ran out of dice.')

        dice = self.reroll
        self.reroll = []
        for i in range(3 - len(dice)):
            dice.append(self.dice.pop(self.rng.randrange(0, len(self.dice))))

        # Roll the three dice.
        outcomes = []
        for die in dice:
            outcome = die.roll()
            outcomes.append(outcome)

            logging.debug('Rolled die {} and got {}'.format(die, outcome))

            if outcome.symbol == BRAIN:
                self.reusable_dice.append(die)
            elif outcome.symbol == FEET:
                self.reroll.append(die)
            else:
                pass # Shotguns are kept in front of the player but we won't need the dice again.

        self.feet = []

        for outcome in outcomes:
            if outcome.symbol == BRAIN:
                self.brains.append(outcome)
            elif outcome.symbol == FEET:
                self.feet.append(outcome)
            elif outcome.symbol == SHOTGUN:
                self.shotguns.append(outcome)

        return {'dice_in_cup': [dice.color for dice in self.dice], 
                'brains': [outcome.color for outcome in self.brains],
                'feet': [outcome.color for outcome in self.feet],
                'shotguns': [outcome.color for outcome in self.shotguns],
                'last_roll': outcomes}
   
class Game:
    def __init__(self, zombies = []):
        self.zombies = zombies if zombies else zombies[:]
        self.player_scores = None
        self.player_order = None
        self.round = 0

    def run(self):
        self.player_scores = dict([(zombie.name, 0) for zombie in self.zombies])
        self.player_order = [zombie.name for zombie in self.zombies]
        random.shuffle(self.player_order)
        logging.debug('Player order: {}'.format(', '.join(self.player_order)))

        # Validate that zombie objects
        for zombie in self.zombies:
            if 'turn' not in dir(zombie):
                logging.error('All zombies need a turn() method.')
                raise GameException('All zombies need a turn() method.')
            if 'name' not in dir(zombie):
                logging.error('All zombies need a name attribute.')

        if len(self.player_order) != len(set(self.player_order)):
            logging.error('Zombies must have unique names.')
            raise GameException('Zombies must have unique names.')
        
        if len(self.zombies) < 2:
            logging.error('Need at least two zombies.')
            raise GameException('Need at least two zombies.')

        logging.info('Zombies {} will be playing a game of Zombie Dice!'.format(', '.join([zombie.name for zombie in self.zombies])))

        # Tell all zombies that a new game is about to commence.
        for zombie in self.zombies:
            if 'new_game' in dir(zombie):
                zombie.new_game()
        
        active_zombies = self.zombies

        while True:
            self.round += 1
            logging.info('Round #{}: scores: {}'.format(self.round, self.player_scores))

            # Tell all zombies that a new turn is about to commence.
            for zombie in active_zombies:
                if 'new_turn' in dir(zombie):
                    zombie.new_turn()
            
            for zombie in active_zombies:
                logging.debug('New turn for {}'.format(zombie.name))

                table = Table()
                
                try:
                    action = True
                    while action:
                        action = zombie.turn(table)

                    if len(table.shotguns) >= 3:
                        self.player_scores[zombie.name] += len(table.brains)
                        logging.debug('Updated player {} score to {}.'.format(zombie.name, self.player_scores[zombie.name]))

                except Exception as e:
                    logging.exception(e)
            
            if max(self.player_scores.values()) >= 13:
                active_zombies = [zombie for zombie in active_zombies if self.player_scores[zombie.name] == max(self.player_scores.values())]
                if len(active_zombies) == 1:
                    logging.info('Game ended with scores: {}.'.format(self.player_scores))
                    return self.player_scores
                else:
                    logging.debug('Tie breaker round initiated for zombies {}'.format(', '.join([zombie.name for zombie in active_zombies])))